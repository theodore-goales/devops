##Vérifier si Terraform est disponible :
terraform

##Créer le fichier de configuration instance.tf :
touch instance.tf


##Afficher la liste des fichiers
ls

##Télécharger et installer le binaire du fournisseur :
terraform init

##Créer un plan d'exécution :
terraform plan

##Appliquer les modifications :
terraform apply

##Inspecter l'état actuel :
terraform show
